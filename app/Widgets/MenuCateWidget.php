<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;

class MenuCateWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $menu_one = DB::table('categories')->where('parent_id', 0)->get();        
        foreach ($menu_one as $item) {
            $menu_two = DB::table('categories')->where('parent_id', $item->id)->get()->toArray();
        }
        return view("widgets.menu_cate_widget", compact('menu_one', 'menu_two'));
    }
}
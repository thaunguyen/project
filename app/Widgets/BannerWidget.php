<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Banner;

class BannerWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $bannerModel = new Banner;
        $banner = $bannerModel->getBanner();

        return view("widgets.banner_widget", compact('banner'));
    }
}

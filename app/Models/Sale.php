<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    CONST RECORDS_PAGE = 5;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'sale_percent',        
        'description',
        'created_at',
        'updated_at',
    ];
    
    /**
     * Get list all Sale
     * 
     * @return Sale $sale
     */
    public function listSale()
    {
        $sale = Sale::select('*');
    	return $sale;
    }
    
    public function getNamebyId()
    {
        $sale = Sale::select('name','id')->pluck('name','id')->prepend('Select a Sale', '0')->toArray();
        return $sale;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    CONST RECORDS_PAGE = 5;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'materials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',                
        'description',
        'created_at',
        'updated_at',
    ];
    
    /**
     * Get list all material
     * 
     * @return Material $material
     */
    public function listMaterial()
    {
        $material = Material::select('*');
    	return $material;
    }
    
    public function getNamebyId()
    {
        $material = Material::select('name','id')->pluck('name','id')->toArray();
        return $material;
    }
}

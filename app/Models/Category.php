<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * This is the model class for table "categories".
 * 
 */
class Category extends Model
{

    CONST RECORDS_PAGE = 5;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'alias',
        'image',
        'order',
        'parent_id',
        'keywords',
        'description',
        'created_at',
        'updated_at',
    ];
    
    /**
     * Get list Category Parent 
     * 
     * @return \Illuminate\Http\Response
     */
    public function getparent()
    {
    	$parent = Category::select('id','name','parent_id')->get()->toArray();
    	return $parent;
    }
    
    /**
     * Get list all Category 
     * 
     * @return Category $cate
     */
    public function listCategory()
    {
    	$cate = Category::select('*');
    	return $cate;
    }
    
    public function listCateParentOne()
    {
        $cateParent = Category::select('*')->where('parent_id', 0)->get();
        return $cateParent;
    }
}

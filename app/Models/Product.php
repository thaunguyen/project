<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    CONST RECORDS_PAGE = 5;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'image',
        'alias',
        'width',
        'height',
        'price',
        'number',
        'keywords',
        'cate_id',
        'material_id',
        'sale_id',
        'description',
        'created_at',
        'updated_at',
    ];
    
    public function pimage()
    {
        return $this->hasMany('App\Models\Product_Image');
    }

    /**
     * Get list all Product: id, name, cate_name, image, number, price;
     * 
     * @return Product $product
     */
    public function listProduct()
    {
        $product = Product::join('categories as c', 'c.id', 'cate_id')
                ->select('products.id', 'products.name', 'c.name as cate_name', 'products.image', 'number', 'price')
                ->orderBy('id', 'ASC');
    	return $product;
    }
    
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'banners';
    
    /**
     * The attributes that are mass assignable.
     * 
     * @var array 
     */
    protected $fillable = [
        'id',
        'title',
        'url',
        'position',
        'created_at',
        'updated_at',
    ];
     
    public function getBanner()
    {
        $banner = Banner::select('*')->get();
        return $banner;
    }
}

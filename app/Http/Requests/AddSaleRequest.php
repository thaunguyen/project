<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required',
            'sale_percent' => 'required|regex:/^[0-9][0-9]{0,15}$/',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required'         => trans('messages.required'),
            'sale_percent.required' => trans('messages.required'),
            'sale_percent.regex'    => trans('messages.regex'),
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:products,name',
            'width' => 'required|regex:/^[0-9][0-9]{0,15}$/',
            'height' => 'required|regex:/^[0-9][0-9]{0,15}$/',
            'image' => 'required',
            'number' => 'required|regex:/^[0-9][0-9]{0,15}$/',
            'price' => 'required|regex:/^[0-9][0-9]{0,15}$/',
            'keywords' => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required'     => trans('messages.required'),
            'name.unique'       => trans('messages.unique'),
            'width.required'    => trans('messages.required'),
            'width.regex'       => trans('messages.regex'),
            'height.required'   => trans('messages.required'),
            'height.regex'      => trans('messages.regex'),
            'image.required'    => trans('messages.required'),
            'number.required'   => trans('messages.required'),
            'number.regex'      => trans('messages.regex'),
            'price.required'    => trans('messages.required'),
            'price.regex'       => trans('messages.regex'),
            'keywords.required' => trans('messages.required'),
        ];
    }
}

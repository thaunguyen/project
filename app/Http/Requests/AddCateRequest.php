<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required',
            'order'       => 'required|regex:/^[0-9][0-9]{0,15}$/',
            'image'       => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'        => trans('messages.required'),
            'order.required'       => trans('messages.required'),
            'order.regex'          => trans('messages.regex'),
            'image.required'       => trans('messages.required'),
            'description.required' => trans('messages.required'),
        ];
    }
}

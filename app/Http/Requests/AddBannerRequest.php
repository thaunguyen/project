<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'    => 'required',
            'position' => 'required|regex:/^[0-9][0-9]{0,15}$/',
            'url'      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'    => trans('messages.required'),
            'position.required' => trans('messages.required'),
            'position.regex'    => trans('messages.regex'),
            'url.required'      => trans('messages.required'),
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * The Category Model
     * @var type 
     */
    protected $cate;

    /**
     * The Product Model
     * @var type 
     */
    protected $product;
    
    /**
     * 
     * @param Category $cate
     * @param Product $product
     */
    public function __construct(Category $cate, Product $product)
    {
        $this->cate    = $cate;
        $this->product = $product;
    }
    
    /**
     * The home page
     * 
     * @return \Illuminate\Support\Facades\Response
     */
    public function index()
    {
        $cateMenu    = $this->cate->listCateParentOne();
        $listProduct = $this->product->all();
        
        return view('frontend.index', compact('cateMenu', 'listProduct'));
    }    
    
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\AddCateRequest;
use File;

class CategoryController extends Controller
{
    
    protected $cate;
    
    /**
     * 
     * @param Category $cate
     */
    public function __construct(Category $cate)
    {
        $this->cate = $cate;
    }
    
    /**
     * Show list Category
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listCate = $this->cate->listCategory()->paginate(Category::RECORDS_PAGE);
        return view('backend.cate.list', compact('listCate'));
    }
    
    /**
     * Show form add Category
     * 
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $parent = $this->cate->getparent();
        return view('backend.cate.add', compact('parent'));
    }
    
    /**
     * Save info Category
     * 
     * @param AddCateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddCateRequest $request)
    {
        $cate = new Category;

        $attribbute  = $request->all();
        $cate->fill($attribbute);
        $cate->alias = changeTitle($attribbute['name']);

        if (!$cate->save()) {
            return redirect()->route('cate.add')->with('errorMsg', trans('cate.notadd'));
        }
        return redirect()->route('cate.list')->with('successMsg', trans('cate.add'));
    }
    
    /**
     * Show info of a Category
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $cate = Category::find($id);

        if (!$cate) {
            return redirect()->route('cate.list')->with('errorMsg', trans('cate.notfind'));
        }

        $parent = $this->cate->getparent();
        return view('backend.cate.view', compact('cate', 'parent'));
    }
    
    /**
     * Show form edit Category
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cate = Category::find($id);

        if (!$cate) {
            return redirect()->route('cate.list')->with('errorMsg', trans('cate.notfind'));
        }
        $parent = $this->cate->getparent();
        return view('backend.cate.edit', compact('cate', 'parent'));
    }
    
    /**
     * Update info of Category
     * 
     * @param int $id
     * @param AddCateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, AddCateRequest $request)
    {
        $cate = Category::find($id);

        if (!$cate) {
            return redirect()->route('cate.list')->with('errorMsg', trans('cate.notfind'));
        }

        $attribbute  = $request->all();
        $cate->fill($attribbute);
        $cate->alias = changeTitle($attribbute['name']);

        if (!$cate->save()) {
            return redirect()->route('cate.add')->with('errorMsg', trans('cate.notupdate'));
        }
        return redirect()->route('cate.list')->with('successMsg', trans('cate.update'));
    }
    
    /**
     * Delete a Category
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $cate = Category::find($id);
        $host = gethost() . '/public';

        $cate_image = str_replace($host, "", $cate->image);

        if (!$cate) {
            return redirect()->route('cate.list')->with('errorMsg', trans('cate.notfind'));
        }

        if (!$cate->delete()) {
            return redirect()->route('cate.list')->with('errorMsg', trans('cate.notdelete'));
        }

        unlink(public_path($cate_image));

        return redirect()->route('cate.list')->with('successMsg', trans('cate.delete'));
    }
}

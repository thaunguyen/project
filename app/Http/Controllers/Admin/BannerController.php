<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Http\Requests\AddBannerRequest;

class BannerController extends Controller
{
    public function index()
    {
        $banner = Banner::all();
        
        return view('backend.banner.view', compact('banner'));
    }
    
    /**
     * Show form add banner
     * 
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.banner.add');
    }
    
    public function store(AddBannerRequest $request)
    {
        $banner = new Banner;
        
        $attribute = $request->all();
        $banner->fill($attribute);
        
        if (!$banner->save()) {
            return redirect()->route('banner.add')->with('errorMsg', trans('banner.notadd'));
        }
        
        return redirect()->route('banner.list')->with('successMsg', trans('banner.add'));
    }
    
    public function edit($id)
    {
        $banner = Banner::find($id);
        
        if (!$banner) {
            return redirect()->route('banner.list')->with('errorMsg', trans('banner.notfind'));
        }
        
        return view('backend.banner.edit', compact('banner'));
    }
    
    public function update($id, AddBannerRequest $request)
    {
        $banner = Banner::find($id);
        
        if (!$banner) {
            return redirect()->route('banner.list')->with('errorMsg', trans('banner.notfind'));
        }
        
        $attribute = $request->all();
        $banner->fill($attribute);
        
        if (!$banner->save()) {
            return redirect()->route('banner.edit')->with('errorMsg', trans('banner.notupdate'));
        }
        
        return redirect()->route('banner.list')->with('successMsg', trans('banner.update'));
    }
    
    public function delete($id)
    {
        $banner     = Banner::find($id);
        
        $host       = gethost() . '/public';
        $url_banner = str_replace($host, "", $banner->url);
        
        if (!$banner) {
            return redirect()->route('banner.list')->with('errorMsg', trans('banner.notfind'));
        }

        if (!$banner->delete()) {
            return redirect()->route('banner.list')->with('errorMsg', trans('banner.notdelete'));
        }
        
        unlink(public_path($url_banner));
        
        return redirect()->route('banner.list')->with('successMsg', trans('banner.delete'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Http\Requests\AddMaterialRequest;

class MaterialController extends Controller
{
    protected $material;
    
    /**
     * 
     * @param Material $material
     */
    public function __construct(Material $material)
    {
        $this->material = $material;
    }
    
    /**
     * Show list Material
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listMaterial = $this->material->listMaterial()->paginate(Material::RECORDS_PAGE);
        return view('backend.material.list', compact('listMaterial'));
    }
    
    /**
     * Show form add Material
     * 
     * @return \Illuminate\Http\Response
     */
    public function add()
    {        
        return view('backend.material.add');
    }
    
    /**
     * Save info Material
     * 
     * @param AddCateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddMaterialRequest $request)
    {
        $material = new Material;

        $attribbute  = $request->all();
        $material->fill($attribbute);
        
        if (!$material->save()) {
            return redirect()->route('material.add')->with('errorMsg', trans('material.notadd'));
        }
        return redirect()->route('material.list')->with('successMsg', trans('material.add'));
    }
        
    /**
     * Show form edit Material
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Material::find($id);

        if (!$material) {
            return redirect()->route('material.list')->with('errorMsg', trans('material.notfind'));
        }        
        return view('backend.material.edit', compact('material'));
    }
    
    /**
     * Update info of Material
     * 
     * @param int $id
     * @param AddMaterialRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, AddMaterialRequest $request)
    {
        $material = Material::find($id);

        if (!$material) {
            return redirect()->route('material.list')->with('errorMsg', trans('material.notfind'));
        }

        $attribbute  = $request->all();
        $material->fill($attribbute);        

        if (!$material->save()) {
            return redirect()->route('material.add')->with('errorMsg', trans('material.notupdate'));
        }
        return redirect()->route('material.list')->with('successMsg', trans('material.update'));
    }
    
    /**
     * Delete a Material
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $material = Material::find($id);
        
        if (!$material) {
            return redirect()->route('material.list')->with('errorMsg', trans('material.notfind'));
        }

        if (!$material->delete()) {
            return redirect()->route('material.list')->with('errorMsg', trans('material.notdelete'));
        }

        return redirect()->route('material.list')->with('successMsg', trans('material.delete'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sale;
use App\Http\Requests\AddSaleRequest;

class SaleController extends Controller
{
    protected $sale;
    
    /**
     * 
     * @param Sale $sale
     */
    public function __construct(Sale $sale)
    {
        $this->sale = $sale;
    }
    
    /**
     * Show list Sale
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listSale = $this->sale->listSale()->paginate(Sale::RECORDS_PAGE);
        return view('backend.sale.list', compact('listSale'));
    }
    
    /**
     * Show form add Sale
     * 
     * @return \Illuminate\Http\Response
     */
    public function add()
    {        
        return view('backend.sale.add');
    }
    
    /**
     * Save info Sale
     * 
     * @param AddCateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddSaleRequest $request)
    {
        $sale = new Sale;

        $attribbute  = $request->all();
        $sale->fill($attribbute);
        
        if (!$sale->save()) {
            return redirect()->route('sale.add')->with('errorMsg', trans('sale.notadd'));
        }
        return redirect()->route('sale.list')->with('successMsg', trans('sale.add'));
    }
        
    /**
     * Show form edit Sale
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sale::find($id);

        if (!$sale) {
            return redirect()->route('sale.list')->with('errorMsg', trans('sale.notfind'));
        }        
        return view('backend.sale.edit', compact('sale'));
    }
    
    /**
     * Update info of Sale
     * 
     * @param int $id
     * @param AddSaleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, AddSaleRequest $request)
    {
        $sale = Sale::find($id);

        if (!$sale) {
            return redirect()->route('sale.list')->with('errorMsg', trans('sale.notfind'));
        }

        $attribbute  = $request->all();
        $sale->fill($attribbute);        

        if (!$sale->save()) {
            return redirect()->route('sale.add')->with('errorMsg', trans('sale.notupdate'));
        }
        return redirect()->route('sale.list')->with('successMsg', trans('sale.update'));
    }
    
    /**
     * Delete a Sale
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $sale = Sale::find($id);
        
        if (!$sale) {
            return redirect()->route('sale.list')->with('errorMsg', trans('sale.notfind'));
        }

        if (!$sale->delete()) {
            return redirect()->route('sale.list')->with('errorMsg', trans('sale.notdelete'));
        }

        return redirect()->route('sale.list')->with('successMsg', trans('sale.delete'));
    }
}

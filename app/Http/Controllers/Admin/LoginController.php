<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\AdminLoginRequest;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    
    public function login()
    {
        return view('backend.login');
    }
    
    /**
     * Login Administrator
     * 
     * @param AdminLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(AdminLoginRequest $request)
    {
         // create array $login
        $login = [
            'name'    => $request->name,
            'password' => $request->password,
        ];
//        dd($login);
        // check login admin
        if (\Auth::guard('admin')->attempt($login)) {
            return redirect()->route('admin.main');
        }
        return redirect()->route('admin.login')
                        ->with('errorMsg', 'sai me mat khau hoac tai khaon roi');
    }
    
    /**
     * Logout Administrator
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
     public function logout()
    {
        //logout user
        \Auth::logout();

        //delte session
        Session::flush();

        //return form login user
        return redirect()->route('admin.login');
    }
}

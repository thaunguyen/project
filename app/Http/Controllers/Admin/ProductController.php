<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Sale;
use App\Models\Material;
use App\Models\Product_Image;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
use File;

class ProductController extends Controller
{
    /**
     * The Product Model
     * @var 
     */
    protected $product;
    
    /**
     * The Product_image Model
     * @var 
     */
    protected $pro_img;

    /**
     * The Category Model
     * @var  
     */
    protected $cate;
    
    /**
     * The Sale Model
     * @var  
     */
    protected $sale;
    
    /**
     * The Material Model
     * @var 
     */
    protected $material;

    /**
     * 
     * @param Product $product
     * @param Category $cate
     * @param Sale $sale
     * @param Material $material
     * @param Product_Image $pro_img
     */
    public function __construct(Product $product, Category $cate, Sale $sale, Material $material, Product_Image $pro_img)
    {
        $this->product  = $product;
        $this->cate     = $cate;
        $this->sale     = $sale;
        $this->material = $material;
        $this->pro_img  = $pro_img;
    }
    
    /**
     * Show list products
     * 
     * @return \Illuminate\Support\Facades\Response
     * 
     */
    public function index()
    {
        $listProduct = $this->product->listProduct()->paginate(Product::RECORDS_PAGE);
        return view('backend.product.list', compact('listProduct'));
    }

    /**
     * Show form add Product
     * 
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $cate     = $this->cate->getparent();
        $sale     = $this->sale->getNamebyId();
        $material = $this->material->getNamebyId();
        return view('backend.product.add', compact('cate', 'sale', 'material'));
    }
    
    /**
     * Save info Product and Product Image Detail
     * 
     * @param AddProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddProductRequest $request)
    {
        $product   = new Product;
        
        $attribute      = $request->all(); 
        $product->fill($attribute);
        $product->alias = changeTitle($attribute['name']);

        if (!$product->save()) {
            return redirect()->route('product.add')->with('errorMsg', trans('product.notadd'));
        }

        $product_id = $product->id;

        if ($request->hasFile('productImageDetail')) {
            foreach ($request->file('productImageDetail') as $file) {
                $product_img = new Product_Image();
                if (isset($file)) {
                    $file_name               = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $extension               = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                    $image_name              = $file_name . '_' . $product_id . '.' . $extension;
                    $product_img->image      = $image_name;
                    $product_img->product_id = $product_id;
                    $file->move('public/uploads/product_detail/', $image_name);
                    $product_img->save();
                }
            }
        }

        return redirect()->route('product.list')->with('successMsg', trans('product.add'));
    }
    
    public function view($id)
    {
        $product = Product::find($id);
        
        if (!$product) {
            return redirect()->route('product.list')->with('errorMsg', trans('product.notfind'));
        }
        $product_detail = Product::find($id)->pimage;
        
        return view('backend.product.view', compact('product', 'product_detail'));
    }

    /**
     * Show form edit info Product
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product       = Product::find($id);
        
        if (!$product) {
            return redirect()->route('product.list')->with('errorMsg', trans('product.notfind'));
        }
        
        $cate          = $this->cate->getparent();
        $sale          = $this->sale->getNamebyId();
        $material      = $this->material->getNamebyId();
        $product_image = Product::find($id)->pimage;
        return view('backend.product.edit', compact('product', 'cate', 'sale', 'material', 'product_image'));
    }
    
    /**
     * Delete Image Product Detail
     * 
     * @param int $id
     * @return string
     */
    public function deleteImgDetail($id)
    { 
        if (Request::ajax()) {
            $idImage       = (int) Request::get('idImage');
            $image_detail = Product_image::find($idImage);
            if (!empty($image_detail)) {
                $img = 'uploads/product_detail/' . $image_detail->image;
                if (File::exists($img)) { 
                    unlink(public_path($img));
                }
                $image_detail->delete();
            }
            return "OK";
        }
    }

    public function update($id, EditProductRequest $request)
    {
        $product = Product::find($id);
        
        if (!$product) {
            return redirect()->route('product.list')->with('errorMsg', trans('product.notfind'));
        }
        
        $attribute = $request->all();
        $product->fill($attribute);
        $product->alias = changeTitle($attribute['name']);
        
        if (!$product->save()) {
            return redirect()->route('product.edit')->with('errorMsg', trans('product.notupdate'));
        }
        
        if (!empty(Request::file('productImgEditDetail'))) {
            foreach (Request::file('productImgEditDetail') as $file) {
                $product_img = new Product_image();
                if (isset($file)) {
                    $file_name               = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $extension               = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                    $image_name              = $file_name . '_' . $id . '.' . $extension;
                    $product_img->image      = $image_name;
                    $product_img->product_id = $id;
                    $file->move('public/uploads/product_detail/', $image_name);
                    $product_img->save();
                }
            }
        }
        return redirect()->route('product.list')->with('success', trans('product.update'));
    }
    
    /**
     * Delete Product by id
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product       = Product::find($id);
        $host          = gethost() . '/public';
        $product_image = str_replace($host, "", $product->image);

        if (!$product) {
            return redirect()->route('product.list')->with('errorMsg', trans('product.notfind'));
        }

        $product_detail = Product::find($id)->pimage->toArray();
        foreach ($product_detail as $item) {
            unlink('public/uploads/product_detail/' . $item["image"]);
        }

        if (!$product->delete()) {
            return redirect()->route('product.list')->with('errorMsg', trans('product.notdelete'));
        }
        unlink(public_path($product_image));
        
        return redirect()->route('product.list')->with('successMsg', trans('product.delete'));
    }
}

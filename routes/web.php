<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/products', function() {
	return view('frontend.product');
});
Route::get('/contact', function() {
	return view('frontend.contacts');
});
Route::get('/cart', function() {
	return view('frontend.cart');
});
Route::get('/single', function() {
	return view('frontend.single');
});
Route::get('admin/login', 'Admin\LoginController@login')->name('admin.login');
Route::post('admin/login', 'Admin\LoginController@postLogin')->name('admin.postlogin');
Route::get('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('index.html', 'Admin\HomeController@index')->name('admin.main');
    Route::group(['prefix' => 'category'], function() {
        Route::get('list', 'Admin\CategoryController@index')->name('cate.list');
        Route::get('view/{id}', 'Admin\CategoryController@view')->name('cate.view');
        Route::get('add', 'Admin\CategoryController@add')->name('cate.add');
        Route::post('add', 'Admin\CategoryController@store')->name('cate.store');
        Route::get('edit/{id}', 'Admin\CategoryController@edit')->name('cate.edit');
        Route::post('update/{id}', 'Admin\CategoryController@update')->name('cate.update');
        Route::post('delete/{id}', 'Admin\CategoryController@delete')->name('cate.delete');
    });
    Route::group(['prefix' => 'sale'], function() {
        Route::get('list', 'Admin\SaleController@index')->name('sale.list');
        Route::get('add', 'Admin\SaleController@add')->name('sale.add');
        Route::post('add', 'Admin\SaleController@store')->name('sale.store');
        Route::get('edit/{id}', 'Admin\SaleController@edit')->name('sale.edit');
        Route::post('update/{id}', 'Admin\SaleController@update')->name('sale.update');
        Route::post('delete/{id}', 'Admin\SaleController@delete')->name('sale.delete');
    });
    Route::group(['prefix' => 'material'], function() {
        Route::get('list', 'Admin\MaterialController@index')->name('material.list');
        Route::get('add', 'Admin\MaterialController@add')->name('material.add');
        Route::post('add', 'Admin\MaterialController@store')->name('material.store');
        Route::get('edit/{id}', 'Admin\MaterialController@edit')->name('material.edit');
        Route::post('update/{id}', 'Admin\MaterialController@update')->name('material.update');
        Route::post('delete/{id}', 'Admin\MaterialController@delete')->name('material.delete');
    });
    Route::group(['prefix' => 'product'], function() {
        Route::get('list', 'Admin\ProductController@index')->name('product.list');
        Route::get('view/{id}', 'Admin\ProductController@view')->name('product.view');
        Route::get('add', 'Admin\ProductController@add')->name('product.add');
        Route::post('add', 'Admin\ProductController@store')->name('product.store');
        Route::get('edit/{id}', 'Admin\ProductController@edit')->name('product.edit');
        Route::post('update/{id}', 'Admin\ProductController@update')->name('product.update');
        Route::post('delete/{id}', 'Admin\ProductController@delete')->name('product.delete');
        Route::get('deleteImg/{id}', 'Admin\ProductController@deleteImgDetail')->name('product.deleteImg');
    });
    Route::group(['prefix' => 'banner'], function() {
        Route::get('list', 'Admin\BannerController@index')->name('banner.list');
        Route::get('add', 'Admin\BannerController@add')->name('banner.add');
        Route::post('add', 'Admin\BannerController@store')->name('banner.store');
        Route::get('edit/{id}', 'Admin\BannerController@edit')->name('banner.edit');
        Route::post('update/{id}', 'Admin\BannerController@update')->name('banner.update');
        Route::post('delete/{id}', 'Admin\BannerController@delete')->name('banner.delete');
    });
});

@foreach($menu_one as $item_level_1)
<div class="col1 me-one">
    <a href=""><h4>{{ $item_level_1->name }}</h4></a>
    <ul>
        <?php $cateParent_level_2 = getMenuCateLevelTwo($item_level_1->id); ?>
        @foreach($cateParent_level_2 as $item_level_2)
        <li><a href="products.html">{{ $item_level_2->name }}</a></li>                                           
        @endforeach
    </ul>
</div>
@endforeach   
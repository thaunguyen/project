@extends('layouts.admin')
@section('title', 'Edit Product')
@section('controller', 'Edit Product')
@section('content')
{{ Form::open(['route' => ['product.update', $product->id], 'enctype' => 'multipart/form-data', 'id' => 'EditProduct']) }}
{{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
<div class="col-lg-8" style="padding-bottom: 80px">
    @include('elements.errors.error') 
    <div class="form-group">
        <label>Category</label>
        <select class="form-control" name="cate_id">
            <option value="">Please Choose Category</option>
            <?php cate_parent($cate, 0, "---", $product->cate_id); ?> 
        </select>
    </div>
    <div class="form-group">
        {{ Form::label('material', 'Material') }}
        {{ Form::select('material_id' , $material, $product->material_id, ['class' => 'form-control', 'placeholder'=>'Please Choose Material'] ) }}
    </div>
    {{ Form::bsText('name', $product->name) }}
    {{ Form::bsText('width', $product->width) }}
    {{ Form::bsText('height', $product->height) }}
    <div class="form-group">
        <label>Image</label><br>
        <input type="hidden" class="form-control" name="image" value="{{ $product->image }}" id="image" placeholder="Please Enter Image" />
        <img src="{{ $product->image }}" id="show-img" width="450" alt="" style="padding-bottom: 5px"><br>
        <a href="#" id="select-img" title="Choose Aavatar" class="btn btn-info btn-sm">Choose Image</a>
        <a href="#" id="remove-img" title="Choose Aavatar" class="btn btn-danger btn-sm">Remove Image</a>
    </div>
    {{ Form::bsText('number', $product->number) }}
    {{ Form::bsText('price', $product->price) }}    
    <div class="form-group">
        {{ Form::label('sale', 'Sale') }}
        {{ Form::select('sale_id' , $sale, $product->sale_id, ['class' => 'form-control'] ) }}
    </div>
    {{ Form::bsText('keywords', $product->keywords) }}
    <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', $product->description, ['id' => 'description', 'class' => 'form-control']) }}    	
    </div>

    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}
</div>
<div class="col-lg-4" style="padding-bottom: 80px">
    <h4>Image detail of Product</h4>
    @foreach($product_image as $key => $item)
    <div class="form-group" id="{!! $key !!}">
        <img src="{!!asset('public/uploads/product_detail/'.$item['image'])!!}" class="img_detail" idHinh="{!! $item['id'] !!}"
             id="{!! $key !!}" />

        <a href="javascript:void(0)" type="button" id="del_img_demo" class="btn btn-danger btn-circle icon_del">
            <i class="fa fa-times"></i>
        </a>   
    </div>
    @endforeach
    <div id="insert"></div>
    <button type="button" class="btn btn-primary" id="addImages">Add Image</button>
</div>
{!! Form::close() !!}
@endsection


@extends('layouts.admin')
@section('title', 'Add Product')
@section('controller', 'Add Product')
@section('content')

{{ Form::open(['route' => ['product.store'], 'enctype' => 'multipart/form-data', 'id' => 'formUpload']) }}
{{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
<div class="col-lg-8" style="padding-bottom: 80px">
    @include('elements.errors.error') 
    <div class="form-group">
        <label>Category</label>
        <select class="form-control" name="cate_id">
            <option value="">Please Choose Category</option>
            <?php cate_parent($cate, 0, "---", old('cate_id')); ?> 
        </select>
    </div>
    <div class="form-group">
        {{ Form::label('material', 'Material') }}
        {{ Form::select('material_id' , $material, null, ['class' => 'form-control', 'placeholder'=>'Please Choose Material'] ) }}
    </div>
    {{ Form::bsText('name') }}
    {{ Form::bsText('width') }}
    {{ Form::bsText('height') }}
    <div class="form-group">
        <label>Image</label><br>
        <input type="hidden" class="form-control" name="image" id="image" placeholder="Please Enter Image" />
        <img src="" id="show-img" width="450" alt="" style="padding-bottom: 5px"><br>
        <a href="#" id="select-img" title="Choose Aavatar" class="btn btn-info btn-sm">Choose Image</a>
        <a href="#" id="remove-img" title="Choose Aavatar" class="btn btn-danger btn-sm">Remove Image</a>
    </div>
    {{ Form::bsText('number') }}
    {{ Form::bsText('price') }}    
    <div class="form-group">
        {{ Form::label('sale', 'Sale') }}
        {{ Form::select('sale_id' , $sale, null, ['class' => 'form-control'] ) }}
    </div>
    {{ Form::bsText('keywords') }}
    <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control']) }}    	
    </div>

    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}


</div>
<div class="col-lg-4" style="padding-bottom: 80px">
    <h4>Choose Image detail for Product</h4>
    @for($i=1;$i<=5;$i++)
    <div class="form-group">
        <label>Image Product Detail {!! $i!!}</label>
        <input type ="file" name = "productImageDetail[]" />
        <div class="box-preview-img"></div>
            
        <div class="output"></div>
    </div>
    @endfor
</div>
{!! Form::close() !!}
@endsection


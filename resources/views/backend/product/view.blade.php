@extends('layouts.admin')
@section('title', 'View Product')
@section('controller', 'View Product' )
@section('content')
<div class="col-lg-8">
    <h3 class="">Infomation Product</h3>
    <table class="table table-bordered" >
        <tbody class="odd gradeX">
            <tr>
                <td width="120px">Id</td>
                <td>{{ $product->id }}</td>
            </tr>
            <tr>
                <td>Category</td>
                <td>{{ $cate = getNameCate($product->cate_id)->name }}</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{{ $product->name }}</td>
            </tr>

            <tr>
                <td>Alias</td>
                <td>{{ $product->alias }}</td>
            </tr>
            <tr>
                <td>Width</td>
                <td>{{ $product->width }} cm</td>
            </tr>
            <tr>
                <td>Height</td>
                <td>{{ $product->height }} cm</td>
            </tr>
            <tr>
                <td>Image</td>
                <td><img src="{{ $product->image }}" alt="" width="250px"></td>
            </tr>
            <tr>
                <td>Material</td>
                <td>{{ $material = getNameMaterial($product->material_id)->name }}</td>
            </tr>
            <tr>
                <td>Sale</td>
                <td><?php
                    $sale = getNameSale($product->sale_id);
                    if ($sale == "") {
                        echo "Không giảm giá";
                    } else {
                        echo $sale->name;
                    }
                    ?></td>
            </tr>
            <tr>
                <td>Number</td>
                <td>{{ $product->number }}</td>
            </tr>
            <tr>
                <td>Price</td>
                <td>{{ $product->price }}</td>
            </tr>		
            <tr>
                <td>KeyWords</td>
                <td>{{ $product->keywords }}</td>
            </tr>
            <tr>
                <td>Description</td>
                <td>{!! $product->description !!}</td>
            </tr>
            <tr>
                <td>Create at</td>
                <td>{{ date("d-m-Y H:i:s", strtotime($product->created_at)) }}

                </td>
            </tr>
            <tr>
                <td>Update at</td>
                <td>{{ date("d-m-Y H:i:s", strtotime($product->updated_at)) }}</td>
            </tr>
        </tbody>	
    </table>	
</div>
<div class="col-lg-4">
    <h3 class="">Image detail of Product</h3>
    @foreach($product_detail as $key => $item)
    <div class="form-group">
        <p>imgae {{ $key+1 }}</p>
        <img src="{!!asset('public/uploads/product_detail/'.$item['image'])!!}" class="img_detail" />   
    </div>
    @endforeach
</div>
@endsection
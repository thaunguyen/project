@extends('layouts.admin')
@section('title', 'List Product')
@section('controller', 'List Product')
@section('content')
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr align="center">
			<th>#</th>
			<th>Product</th>
			<th>Image</th>
                        <th>Category</th>
			<th>Numbert</th>
                        <th>Price</th>
			<th>Delete</th>
			<th>Edit</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; ?>
		@foreach($listProduct as $item)
		<tr class="odd gradeX" align="">
			<td>{{ $i++ }}</td>
			<td><a href="{{ route('product.view', $item->id) }}" title="">{{ $item->name }}</a></td>
			<td><img src="{{ $item->image }}" alt="" width="70px"></td>			
			<td>{{ $item->cate_name }}</td>
                        <td>{{ $item->number }}</td>
                        <th>{{ $item->price }}</th>
			<td class="center">
				{{ link_to_route('product.delete', 'Delete', [$item->id] ,['class' => 'btn btn-danger', 'id' => $item->id, 'data-method' => 'delete', 'data-confirm' => trans('product.msgdelete'), 'data-token' => csrf_token() ]) }}
			</td>
			<td class="center">
				{{ link_to_route('product.edit', 'Edit', [$item->id], ['class' => 'btn btn-info']) }}
			</td>
			</tr>
			@endforeach
		</tbody>
</table>
<div class="pagination pull-right">
        {{ $listProduct->links() }}
    </div>
@endsection


@extends('layouts.blanks')
@section('title', 'Login Administrator'))
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="col-md-12">
                @include('elements.errors.error') <!-- catch error -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Login Administrator</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(['route' => 'admin.postlogin'], ['method' => 'Post']) }}
                <div class="form-group">
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Please Enter UserName']) }}
                </div>
                <div class="form-group">
                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Please Enter Password']) }}
                </div>
                {{ Form::submit('Login', ['class' => 'btn btn-lg btn-success btn-block'])}}
                {{ Form::close() }}
            </div>
        </div>
        </div>
    </div>
</div>
</div>
@endsection


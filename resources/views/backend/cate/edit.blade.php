@extends('layouts.admin')
@section('title', 'Edit Category')
@section('controller', 'Edit Category')
@section('content')
<div class="col-lg-8" style="padding-bottom:120px">
    @include('elements.errors.error') 
    {!! Form::open(['route' => ['cate.update', $cate->id]]) !!}
    {{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
    <div class="form-group">
    	<label>Category Parent</label>
    	<select class="form-control" name="parent_id">
    		<option value="0">Please Choose Category</option>
    		<?php cate_parent($parent, 0 ,"---", $cate->parent_id) ?> 
    	</select>
    </div>
    {{ Form::bsText('name', $cate->name) }}
    <div class="form-group">
    <label>Image</label><br>
    	<input type="hidden" class="form-control" name="image" id="image" value="{{ $cate->image }}" placeholder="Please Enter Image" />
    	<img src="{{ $cate->image }}" id="show-img" width="450" alt="" style="padding-bottom: 5px"><br>
    	<a href="#" id="select-img" title="Choose Aavatar" class="btn btn-info btn-sm">Choose Image</a>
    	<a href="#" id="remove-img" title="Choose Aavatar" class="btn btn-danger btn-sm">Remove Image</a>
    </div>
    {{ Form::bsText('order', $cate->order) }}
    {{ Form::bsText('keywords', $cate->keywords) }}
    <div class="form-group">
    	{{ Form::label('description', 'Description') }}
    	{{ Form::textarea('description', $cate->description, ['id' => 'description', 'class' => 'form-control']) }}    	
    </div>
    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}
    {!! Form::close() !!}

</div>
@endsection
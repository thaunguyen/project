@extends('layouts.admin')
@section('title', 'View Category')
@section('controller', 'View Category' )
@section('content')
<div class="col-lg-10">
<table class="table table-bordered" >
	<tbody class="odd gradeX">
		<tr>
			<td width="120px">Id</td>
			<td>{{ $cate->id }}</td>
		</tr>
		<tr>
			<td>Name</td>
			<td>{{ $cate->name }}</td>
		</tr>
		<tr>
			<td>Alias</td>
			<td>{{ $cate->alias }}</td>
		</tr>
		<tr>
			<td>Image</td>
			<td><img src="{{ $cate->image }}" alt="" width="250px"></td>
		</tr>

		<tr>
			<td>Order</td>
			<td>{{ $cate->order }}</td>
		</tr>
		<tr>
			<td>Category Parent</td>
			<td>
				<?php 
					$nameParent = getNameCateParent($cate->parent_id);
					if ($nameParent == null) {
						echo "None";
					} else
					echo $nameParent->name;					
				?>
			</td>
		</tr>
		<tr>
			<td>KeyWords</td>
			<td>{{ $cate->keywords }}</td>
		</tr>
		<tr>
			<td>Description</td>
			<td>{!! $cate->description !!}</td>
		</tr>
		<tr>
			<td>Create at</td>
			<td>{{ date("d-m-Y H:i:s", strtotime($cate->created_at)) }}
				
			</td>
		</tr>
		<tr>
			<td>Update at</td>
			<td>{{ date("d-m-Y H:i:s", strtotime($cate->updated_at)) }}</td>
		</tr>
	</tbody>	
</table>	
</div>
@endsection
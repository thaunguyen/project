@extends('layouts.admin')
@section('title', 'Edit Material')
@section('controller', 'Edit Material')
@section('content')
<div class="col-lg-8" style="padding-bottom: 80px">
    @include('elements.errors.error') 
    {!! Form::open(['route' => ['material.update', $material->id]]) !!}
    {{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
   
    {{ Form::bsText('name', $material->name) }}
    {{ Form::bsText('description', $material->description) }}
   
    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}
    {!! Form::close() !!}

</div>
@endsection
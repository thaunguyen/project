@extends('layouts.admin')
@section('title', 'List Material')
@section('controller', 'List Material')
@section('content')
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr align="center">
			<th>#</th>
			<th>Material</th>			
			<th>Description</th>
			<th>Delete</th>
			<th>Edit</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; ?>
		@foreach($listMaterial as $item)
		<tr class="odd gradeX" align="">
			<td>{{ $i++ }}</td>
			<td><a href="" title="">{{ $item->name }}</a></td>			
			<td>{{ $item->description }}</td>
			<td class="center">
				{{ link_to_route('material.delete', 'Delete', [$item->id] ,['class' => 'btn btn-danger', 'id' => $item->id, 'data-method' => 'delete', 'data-confirm' => trans('material.msgdelete'), 'data-token' => csrf_token() ]) }}
			</td>
			<td class="center">
				{{ link_to_route('material.edit', 'Edit', [$item->id], ['class' => 'btn btn-info']) }}
			</td>
			</tr>
			@endforeach
		</tbody></table>
<div class="pagination pull-right">
        {{ $listMaterial->links() }}
    </div>
@endsection



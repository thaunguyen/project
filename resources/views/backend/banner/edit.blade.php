@extends('layouts.admin')
@section('title', 'Edit Banner')
@section('controller', 'Edit Banner')
@section('content')
<div class="col-lg-8" style="padding-bottom: 80px">
    @include('elements.errors.error') 
    {!! Form::open(['route' => ['banner.update', $banner->id]]) !!}
    {{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
    {{ Form::bsText('title', $banner->title) }}
    <div class="form-group">
    <label>Url</label><br>
    <input type="hidden" class="form-control" name="url" id="image" value="{{ $banner->url }}" placeholder="Please Enter Image" />
    	<img src="{{ $banner->url }}" id="show-img" width="450" alt="" style="padding-bottom: 5px"><br>
    	<a href="#" id="select-img" title="Choose Aavatar" class="btn btn-info btn-sm">Choose Image</a>
    	<a href="#" id="remove-img" title="Choose Aavatar" class="btn btn-danger btn-sm">Remove Image</a>
    </div>
    {{ Form::bsText('position', $banner->position) }}    
    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}
    {!! Form::close() !!}

</div>
@endsection


@extends('layouts.admin')
@section('title', 'List Banner')
@section('controller', 'List Banner')
@section('content')
<div class="col-lg-5" style="padding-bottom: 20px">
    <button type="button" class="btn btn-primary" id="review-banner">Review</button>
</div>
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr align="center">
            <th>#</th>
            <th>Title</th>
            <th>Url</th>
            <th>Position</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1; ?>
        @foreach($banner as $item)
        <tr class="odd gradeX" align="">
            <td>{{ $i++ }}</td>
            <td><a href="" title="">{{ $item->title }}</a></td>
            <td><img src="{{ $item->url }}" alt="" width="70px"></td>			
            <td>{{ $item->position }}</td>
            <td class="center">
                {{ link_to_route('banner.delete', 'Delete', [$item->id] ,['class' => 'btn btn-danger', 'id' => $item->id, 'data-method' => 'delete', 'data-confirm' => trans('banner.msgdelete'), 'data-token' => csrf_token() ]) }}
            </td>
            <td class="center">
                {{ link_to_route('banner.edit', 'Edit', [$item->id], ['class' => 'btn btn-info']) }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="modal fade bs-example-modal-lg" id="review-banner-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Review Banner</h4>
      </div>
        <div class="modal-body">
            <div class="bnr" id="home">
                <div  id="top" class="callbacks_container">
                    <ul class="rslides" id="slider-review">
                        @foreach($banner as $item) 
                        <li>
                            <img src="{{ $item->url }}" alt="{{ $item->title }}" height="400px" width="800px"/>
                        </li>
                        @endforeach                        
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>      
    </div>
  </div>
</div>
@endsection

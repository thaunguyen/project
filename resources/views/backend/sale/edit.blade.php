@extends('layouts.admin')
@section('title', 'Edit Sale')
@section('controller', 'Edit Sale')
@section('content')
<div class="col-lg-8" style="padding-bottom: 80px">
    @include('elements.errors.error') 
    {!! Form::open(['route' => ['sale.update', $sale->id]]) !!}
    {{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
   
    {{ Form::bsText('name', $sale->name) }}    
    {{ Form::bsText('sale_percent', $sale->sale_percent) }}
    {{ Form::bsText('description', $sale->description) }}
   
    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}
    {!! Form::close() !!}

</div>
@endsection
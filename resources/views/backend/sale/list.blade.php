@extends('layouts.admin')
@section('title', 'List Sale')
@section('controller', 'List Sale')
@section('content')
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	<thead>
		<tr align="center">
			<th>#</th>
			<th>Sale</th>
			<th>Sale Percent</th>
			<th>Description</th>
			<th>Delete</th>
			<th>Edit</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; ?>
		@foreach($listSale as $item)
		<tr class="odd gradeX" align="">
			<td>{{ $i++ }}</td>
			<td><a href="" title="">{{ $item->name }}</a></td>
			<td>{{ $item->sale_percent }}%</td>			
			<td>{{ $item->description }}</td>
			<td class="center">
				{{ link_to_route('sale.delete', 'Delete', [$item->id] ,['class' => 'btn btn-danger', 'id' => $item->id, 'data-method' => 'delete', 'data-confirm' => trans('sale.msgdelete'), 'data-token' => csrf_token() ]) }}
			</td>
			<td class="center">
				{{ link_to_route('sale.edit', 'Edit', [$item->id], ['class' => 'btn btn-info']) }}
			</td>
			</tr>
			@endforeach
		</tbody></table>
<div class="pagination pull-right">
        {{ $listSale->links() }}
    </div>
@endsection


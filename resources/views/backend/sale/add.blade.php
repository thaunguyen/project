@extends('layouts.admin')
@section('title', 'Add Sale')
@section('controller', 'Add Sale')
@section('content')
<div class="col-lg-8" style="padding-bottom: 80px">
    @include('elements.errors.error') 
    {!! Form::open(['route' => ['sale.store']]) !!}
    {{ Form::component('bsText', 'elements.components.form.text', ['name', 'value' , 'attributes'=>['placeholder'=>'Please Enter']])}}
   
    {{ Form::bsText('name') }}    
    {{ Form::bsText('sale_percent') }}
    {{ Form::bsText('description') }}
   
    {{ Form::submit('Submit',['class' => 'btn btn-default'])}}
    {{ Form::reset('Reset',['class' => 'btn btn-default'])}}
    {!! Form::close() !!}

</div>
@endsection
@extends('layouts.app')
@section('content')
<!--start-breadcrumbs-->
<div class="breadcrumbs">
    <div class="container">
        <div class="breadcrumbs-main">
            <ol class="breadcrumb">
                <li><a href="index.html">Trang chủ</a></li>
                <li class="active">Sản Phẩm</li>
            </ol>
        </div>
    </div>
</div>
<!--end-breadcrumbs-->
<!--prdt-starts-->
<div class="prdt"> 
    <div class="container">
        <div class="prdt-top">
            <div class="col-md-9 prdt-left">
                <div class="product-one">
                    <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-1.jpg" alt="" /></a>
                            <div class="product-bottom">
                                <h3>Bát ăn cơm</h3>
                                <p>Explore Now</p>
                                <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                            </div>
                            <div class="srch srch1">
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-2.jpg" alt="" /></a>
                            <div class="product-bottom">
                                <h3>Lọ cổ bé</h3>
                                <p>Explore Now</p>
                                <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                            </div>
                            <div class="srch srch1">
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-3.jpg" alt="" /></a>
                            <div class="product-bottom">
                                <h3>Bát bộ 3</h3>
                                <p>Explore Now</p>
                                <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                            </div>
                            <div class="srch srch1">
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="product-one">
                    <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-4.jpg" alt="" /></a>
                            <div class="product-bottom">
                                <h3>Bát ô van</h3>
                                <p>Explore Now</p>
                                <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                            </div>
                            <div class="srch srch1">
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="{{ URL::to('/single')}}" class="mask"><img class="img-responsive zoom-img" src="images/p-5.jpg" alt="" /></a>
                            <div class="product-bottom">
                                <h3>Bát sỏ đũa</h3>
                                <p>Explore Now</p>
                                <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                            </div>
                            <div class="srch srch1">
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 product-left p-left">
                        <div class="product-main simpleCart_shelfItem">
                            <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-7.jpg" alt="" /></a>
                            <div class="product-bottom">
                                <h3>Lọ đơn</h3>
                                <p>Explore Now</p>
                                <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                            </div>
                            <div class="srch srch1">
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>	
            </div>	
            <div class="col-md-3 prdt-right">
                @include('elements.filter')
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--product-end-->
@endsection

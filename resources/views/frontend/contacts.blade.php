@extends('layouts.app')
@section('content')
<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="index.html">Trang chủ</a></li>
					<li class="active">Liên Hệ</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<!--contact-start-->
	<div class="contact">
		<div class="container">
			<div class="contact-top heading">
				<h2>Góp Ý</h2>
			</div>
				<div class="contact-text">
				<div class="col-md-3 contact-left">
						<div class="address">
							<h5>Địa chỉ</h5>
							<p>Công Ty TNHH Vũ Việt Anh, 
							<span>Đường 57C, Yên Tiến,</span>
							Ý Yên, Nam Định</p>
						</div>
						<div class="address">
							<h5>Liên Hệ</h5>
							<p>SĐT: 0987496968, 
							<span>Fax:0350 3968699</span>
							Email: <a href="mailto:example@email.com">vietanhbambo@gmail.com</a></p>
						</div>
					</div>
					<div class="col-md-9 contact-right">
						<form>
							<input type="text" placeholder="Name">
							<input type="text" placeholder="Phone">
							<input type="text"  placeholder="Email">
							<textarea placeholder="Message" required=""></textarea>
							<div class="submit-btn">
								<input type="submit" value="Gửi">
							</div>
						</form>
					</div>	
					<div class="clearfix"></div>
				</div>
		</div>
	</div>
	<!--contact-end-->
	<!--map-start-->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3742.2677856092355!2d106.03209651453712!3d20.28918008640366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313676f6aaaaaaab%3A0xa381285b6330ffc!2sC%C3%B4ng+ty+Tnhh+Vu+Vi%C2%BFt+Anh!5e0!3m2!1svi!2s!4v1484671869727"  style="border:0" ></iframe>
	</div>
	<!--map-end-->
@endsection
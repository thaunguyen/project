@extends('layouts.app')
@section('content')
@include('elements.banner')
@include('elements.about')
<!--product-starts-->
<div class="product"> 
    <div class="container">
        <div class="product-top">
            <div class="product-one">
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-1.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Bát ăn cơm</h3>
                            <p>{{ trans('homepage.explore') }}</p>
                            <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-2.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Lọ cổ bé</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-3.jpg"  alt="" /></a>
                        <div class="product-bottom">
                            <h3>Bát bộ 3</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a><span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-4.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Bát ô van</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a><span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="product-one">
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="{{ URL::to('/single')}}" class="mask"><img class="img-responsive zoom-img" src="images/p-5.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Bát sỏ đũa</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="{{ URL::to('/single')}}" class="mask"><img class="img-responsive zoom-img" src="images/p-6.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Bát sỏ đũa</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-7.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Lọ đơn</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 product-left">
                    <div class="product-main simpleCart_shelfItem">
                        <a href="single.html" class="mask"><img class="img-responsive zoom-img" src="images/p-8.jpg" alt="" /></a>
                        <div class="product-bottom">
                            <h3>Lọ đôi</h3>
                            <p>Explore Now</p>
                            <h4><a class="item_add" href="#"><i></i></a> <span class=" item_price">50.000 VNĐ</span></h4>
                        </div>
                        <div class="srch">
                            <span>-50%</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>					
        </div>
    </div>
</div>
<!--product-end-->
@endsection
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="icon"  href="{!! asset('public/uploads/icon/manager.png')!!}">
    <!-- Bootstrap -->
    <link href="{{ url('public/admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('public/admin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ url('public/admin/css/custom.min.css') }}" rel="stylesheet">
    <link href="{{ url('public/admin/css/admin.css') }}" rel="stylesheet">
    <link href="{{ url('public/admin/css/slider.css') }}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa  fa-hand-spock-o"></i> <span>Management!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/img.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>@if(isset(Auth::guard('admin')->user()->name))
                      {{ Auth::guard('admin')->user()->name }}
                      @endif</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a href="{{ route('admin.main') }}"><i class="fa fa-home"></i> Home </a>                    
                  </li>
                  <li><a><i class="fa fa-edit"></i> Category <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('cate.add') }}">Add Category</a></li>
                      <li><a href="{{ route('cate.list') }}">List Category</a></li>                    
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Sale <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('sale.add') }}">Add Sale</a></li>
                      <li><a href="{{ route('sale.list') }}">List Sale</a></li>                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Material <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('material.add') }}">Add Material</a></li>
                      <li><a href="{{ route('material.list') }}">List Material</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Product <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('product.add')}}">Add Product</a></li>
                      <li><a href="{{route('product.list')}}">List Product</a></li>                      
                    </ul>
                  </li>  
                  <li><a><i class="fa fa-bar-chart-o"></i> Banner <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('banner.add')}}">Add Banner</a></li>
                      <li><a href="{{route('banner.list')}}">List Banner</a></li>                      
                    </ul>
                  </li>  
                </ul>
              </div>          
            </div>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <i class="fa fa-cog" aria-hidden="true"></i>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <i class="fa fa-arrows-alt" aria-hidden="true"></i>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <i class="fa fa-low-vision" aria-hidden="true"></i>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <i class="fa fa-power-off" aria-hidden="true"></i>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.png" alt="">
                    @if(isset(Auth::guard('admin')->user()->name))
                      {{ Auth::guard('admin')->user()->name }}
                      @endif
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">
                    @if(isset(Auth::guard('admin')->user()->name))
                      {{ Auth::guard('admin')->user()->name }}
                      @endif</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">@yield('controller')</h3>
                </div>
                <div class="col-lg-12">
                    @include('elements.errors.messages')
                </div>
                @yield('content')
            </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Công Ty TNHH Vũ Việt Anh - Đường 57C, Yên Tiến, Ý Yên, Nam Định <a href="#">bomayhan.com</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    @include('elements.admin.modal')
    <!-- jQuery -->
    <script src="{{ url('public/admin/js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ url('public/admin/js/bootstrap.min.js') }}"></script>      
    <!-- Custom Theme Scripts -->
    <script src="{{ url('public/admin/js/custom.min.js') }}"></script>
    <script src="{{ url('public/js/responsiveslides.min.js') }}"></script>
    <script src="{{ url('public/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ url('public/admin/js/admin.js') }}"></script>
    <script src="{{ url('public/admin/js/postlink.js') }}"></script>
  </body>
</html>

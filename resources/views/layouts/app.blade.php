<!DOCTYPE html>
<html>
<head>
<title>SHOP</title>
<link rel="icon"  href="{!! asset('public/uploads/icon/basket.png')!!}">
<link href="{{ url('public/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<script src="{{ url('public/js/jquery-1.11.0.min.js') }}"></script>
<!--Custom-Theme-files-->
<!--theme-style-->

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--start-menu-->
<script src="{{ url('public/js/simpleCart.min.js') }}"> </script>

<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<link href="{{ url('public/css/memenu.css') }}" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ url('public/js/memenu.js') }}"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>	
<!--dropdown-->
<script src="{{ url('public/js/jquery.easydropdown.js') }}"></script>
<script src="{{ url('public/js/responsiveslides.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{ url('public/css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('public/css/owl.theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('public/css/owl.transitions.css') }}">
<script src="{{ url('public/js/owl.carousel.min.js') }}"></script>
<link href="{{ url('public/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
</head>
<body> 
	@include('elements.header')
	@yield('content')
	@include('elements.footer')

<script src="{{ url('public/js/main.js') }}"></script>
</body>
</html>

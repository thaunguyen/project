<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Cua Hang Thu Cong My Nghe Viet Anh">
        <meta name="author" content="Nguyen Quoc Thau">
        <title>@yield('title')</title>
        <link rel="icon"  href="{!! asset('public/uploads/icon/manager.png')!!}">
        <!-- Bootstrap -->
        <link href="{{ url('public/admin/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ url('public/admin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ url('public/admin/css/custom.min.css') }}" rel="stylesheet">
        
    </head>
    <body>
        <div id="wrapper" style="padding-top: 150px">
            <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @include('elements.errors.messages')
            </div>
            @yield('content')
        </div>
        </div>
        <script src="{{ url('public/admin/js/admin.js') }}"></script>
    </body>
</html>


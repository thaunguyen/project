<!--top-header-->
<div class="top-header">
    <div class="container">
        <div class="top-header-main">
            <div class="col-md-10 top-header-left">            
                <marquee scrollamount="4"> Công ty TNHH Vũ Việt Anh chuyên bán những đồ thủ công mỹ nghệ bền, đẹp, giá cả hợp lý. Mọi chi tiết liên hệ: 0987496968</marquee>
                <div class="clearfix"></div>            
            </div>
            <div class="col-md-2 top-header-left">
                <div class="cart box_1">
                    <a href="{{ URL::to('/cart')}}">
                        <div class="total">
                            <span class="simpleCart_total"></span></div>
                        <img src="images/cart-1.png" alt="" />
                    </a>
                    <p><a href="javascript:void();" class="simpleCart_empty">Giỏ Hàng</a></p>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<l
<!--top-header-->
<!--start-logo-->
<div class="logo">
    <a href="{{ URL::to('/') }}"><h1>{{ trans('homepage.label_homepage') }}</h1></a>
</div>
<!--start-logo-->
<!--bottom-header-->
<div class="header-bottom">
    <div class="container">
        <div class="header">
            <div class="col-md-7 header-left">
                <div class="top-nav">
                    <ul class="memenu skyblue"><li class="active"><a href="{{ URL::to('/')}}">{{ trans('homepage.H001') }}</a></li>
                        <li class="grid"><a href="{{ URL::to('/products')}}">{{ trans('homepage.H002') }}</a>
                            <div class="mepanel">
                                <div class="row">     
                                    @widget('MenuCateWidget')                                 
                                </div>
                            </div>
                        </li>
                        <li class="grid"><a href="#">Giới Thiệu</a>
                        </li>
                        <li class="grid"><a href="{{ URL::to('/contact') }}">{{ trans('homepage.H004') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-5 header-right">
            {{ Form::open() }}               
                <div class="form-group input-group">
                    {{ Form::text('search', null, ['class' => 'form-control', 'id' => 'search', 'placeholder' => 'Tìm kiếm sản phẩm']) }}        
                    <span class="input-group-btn">          
                        {{ Form::submit('&#128269;', ['class' =>'btn btn-default']) }}
                    </span>
                </div>     
            {{ Form::close() }}            
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--bottom-header-->
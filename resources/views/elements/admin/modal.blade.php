<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" width="756">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">File Manager</h4>
      </div>
      <div class="modal-body">
        <iframe  width="100%" height="550" frameborder="0" src="{{url('filemanager/dialog.php?type=1&field_id=image')}}"> </iframe>
      </div>
    </div>
  </div>
</div>
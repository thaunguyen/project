<!--about-starts-->
<div class="about"> 
	<div class="container">
		<div class="about-top grid-1" id="menu-carousel">
		@foreach($cateMenu as $item)
			<div class="about-left">				
				<figure class="effect-bubba">
					<img class="img-responsive" src="{{ $item->image}}" alt="" style="height :250px; width:350px "  />
					<figcaption>
						<h2>{{ $item->name }}</h2>
						<p>{!! $item->description !!}</p>	
					</figcaption>			
				</figure>			
			</div>	
			@endforeach		
			<div class="clearfix"></div>
		</div>
	</div>
	<script>
	// You can also use "$(window).load(function() {"
	$(function () {
	    // Slideshow 4
	    $("#menu-carousel").owlCarousel({
	      autoPlay: 10000,
	      items: 3,
	      pagination:false,
	      navigation : true,
	      navigationText :["<",">"]
	  });

	});
	</script>
</div>
<!--about-end-->
@if (session()->has('successMsg'))
<div class="alert alert-success }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! session('successMsg') !!}
</div>
@endif
@if (session()->has('errorMsg'))
<div class="alert alert-danger }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! session('errorMsg') !!}
</div>
@endif
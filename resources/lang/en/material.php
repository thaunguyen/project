<?php 

return [
	'notadd'    => 'Error !! Not add Material',
	'add'       => 'Success !! Add Material',
	'notfind'   => 'Error !! Not found Material',
	'notupdate' => 'Error !! Not update Material',
	'update'    => 'Success !! Updated Material',
	'notdelete' => 'Error !! Not delete Material',
	'delete'    => 'Success !! Deleted Material', 


	//messages
	'msgdelete' => 'You are sure to delete',
];

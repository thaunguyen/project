<?php 

return [
	'notadd'    => 'Error !! Not add Sale',
	'add'       => 'Success !! Add Sale',
	'notfind'   => 'Error !! Not found Sale',
	'notupdate' => 'Error !! Not update Sale',
	'update'    => 'Success !! Updated Sale',
	'notdelete' => 'Error !! Not delete Sale',
	'delete'    => 'Success !! Deleted Sale', 


	//messages
	'msgdelete' => 'You are sure to delete',
];
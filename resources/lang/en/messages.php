<?php

return [
    'required' => 'The ":attribute" not null',
    'regex'    => 'The ":attribute" wrong syntax',
    'unique' => 'The ":attribute" already exists',
];

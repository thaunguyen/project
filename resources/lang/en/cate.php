<?php 

return [
	'notadd'    => 'Error !! Not add Category',
	'add'       => 'Success !! Add Category',
	'notfind'   => 'Error !! Not found Category',
	'notupdate' => 'Error !! Not update Category',
	'update'    => 'Success !! Updated Category',
	'notdelete' => 'Error !! Not delete Category',
	'delete'    => 'Success !! Deleted Category', 


	//messages
	'msgdelete' => 'You are sure to delete',
];

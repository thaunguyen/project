<?php 

return [
	'notadd'    => 'Error !! Not add Product',
	'add'       => 'Success !! Add Product',
	'notfind'   => 'Error !! Not found Product',
	'notupdate' => 'Error !! Not update Product',
	'update'    => 'Success !! Updated Product',
	'notdelete' => 'Error !! Not delete Product',
	'delete'    => 'Success !! Deleted Product', 


	//messages
	'msgdelete' => 'You are sure to delete',
];


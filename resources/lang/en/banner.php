<?php 

return [
	'notadd'    => 'Error !! Not add Banner',
	'add'       => 'Success !! Add Banner',
	'notfind'   => 'Error !! Not found Banner',
	'notupdate' => 'Error !! Not update Banner',
	'update'    => 'Success !! Updated Banner',
	'notdelete' => 'Error !! Not delete Banner',
	'delete'    => 'Success !! Deleted Banner', 


	//messages
	'msgdelete' => 'You are sure to delete',
];

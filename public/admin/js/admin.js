var host = window.location.href;
host = host.split('admin');

tinymce.init({
    selector: 'textarea#description',
    height: "300px",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools code fullscreen"
    ],
    toolbar1: "undo redo bold italic underline strikethrough cut copy paste| alignleft aligncenter alignright alignjustify bullist numlist outdent indent blockquote searchreplace | styleselect formatselect fontselect fontsizeselect ",
    toolbar2: "table | hr removeformat | subscript superscript | charmap emoticons ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft | link unlink anchor image media | insertdatetime preview | forecolor backcolor print fullscreen code ",
    image_advtab: true,
    menubar: false,
    toolbar_items_size: 'small',
    relative_urls: false,
    remove_script_host: false,
    external_filemanager_path: host[0] + "filemanager/",
    filemanager_title: "Quan ly file",
    external_plugins: {"filemanager": host[0] + "filemanager/plugin.min.js"},
    file_browser_callback: 'RoxyFileBrowser',
    file_browser_callback_types: 'file image media'
});

$(document).ready(function () {
    $("a#select-img").click(function (event) {
        event.preventDefault();
        $("#myModal").modal('show');
        $("#myModal").on('hide.bs.modal', function (e) {
            var imgUrl = $('input#image').val();
            $('img#show-img').attr('src', imgUrl);
            // alert(host[0]);
        });
    });
});

$(document).ready(function () {
    $("a#remove-img").click(function (event) {
        event.preventDefault();
        $('input#image').val('');
        $('img#show-img').attr('src', '');
    });
});

$(document).ready(function () {
    $('div.alert').delay(3000).slideUp();
});

$(document).ready(function () {
    $("#addImages").click(function () {
        $("#insert").append('<div class="form-group"><label>Image Product Detail</label><input type="file" name="productImgEditDetail[]"/></div>');
    });
});

$(document).ready(function () {
    $("button#review-banner").click(function (event) {
        event.preventDefault();
        $("#review-banner-modal").modal('show');

    });
});

$(function () {
    // Slideshow 4
    $("#slider-review").responsiveSlides({
        auto: true,
        pager: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

});

$(document).ready(function () {
    $("a#del_img_demo").on('click', function () {
        var url = host[0] + "admin/product/deleteImg/";
        var _token = $("form[name='EditProduct']").find("input[name='_token']").val();
        var idImage = $(this).parent().find("img").attr("idHinh");
        var img = $(this).parent().find("img").attr("src");
        var rid = $(this).parent().find("img").attr("id");

        $.ajax({
            url: url + idImage,
            type: 'GET',
            cache: false,
            data: {"_token": _token, "idImage": idImage, "urlImage": img},
            success: function (data) {
                if (data == "OK")
                {
                    $("#" + rid).remove();
                } else
                {
                    alert('error!please contact admin');
                }
            }
        });
    });
});

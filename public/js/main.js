$(document).ready(function() {
	$("#menu-carousel").owlCarousel({
		autoPlay: 4000,
		items:3,
		pagination:false,
		navigation : true,
		navigationText :["<",">"]
	});
});